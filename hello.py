import json
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hola Gato!'

@app.route('/get', methods=['GET'])
def get_something():
    return 'gotten'


@app.route('/name/<name>', methods=['GET'])
def get_json(name='default_name'):
    ret = {}
    ret['name'] = name
    ret_json = json.JSONEncoder().encode(ret)
    return ret_json
